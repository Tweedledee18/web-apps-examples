import {useEffect, useState} from "react";
import TodoItem from "../../models/todo_item";
import axios from 'axios';
import Item from "./item";

const TodoList = () => {
    const [items, setItems] = useState<TodoItem[]>([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const getItems = async () => {

            let {data} = await axios.get<TodoItem[]>(
                "/api/todo-list",
                {
                    headers: {
                        Accept: 'application/json',
                    },
                },
            )
            setItems(data);
            setIsLoading(false);
        }

        getItems();
    }, []);

    if (isLoading) return <span>Загрузка...</span>

    return (
        <ul>
            {items.map(i => (<Item key={i.id} id={i.id} title={i.title}/>))}
        </ul>
    )
}

export default TodoList;

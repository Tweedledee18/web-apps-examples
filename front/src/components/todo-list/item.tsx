import {useState} from "react";
import '../../App.css'

interface ItemProps {
    id: number,
    title: string,
}

const Item = ({id, title}: ItemProps) => {
    const [isTicked, setIsTicked] = useState(false);
    return (
        <li>
            <label className={isTicked ? 'strikethrough': ''}>{title}</label>
            <input type="checkbox"  onChange={e => setIsTicked(e.target.checked)} ></input>
        </li>
    )
}

export default Item;

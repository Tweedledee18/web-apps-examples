import TodoList from "../components/todo-list/list";

const TodoListPage = () => {
    return (
        <>
            <h1>Список дел на сегодня</h1>
            <TodoList/>
        </>
    )
}

export default TodoListPage;

import {Link, Outlet} from "react-router-dom";
import React from "react";

const RootPage = () => {
    return (
        <>
            <ul>
                <li><Link to={"/"}>Главная</Link></li>
                <li><Link to={"/todo-list"}>Список дел</Link></li>
                <li><Link to={"/about"}>О нас</Link></li>
            </ul>
            <div id="detail">
                <Outlet/>
            </div>
        </>
    )
}

export default RootPage;

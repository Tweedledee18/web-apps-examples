function onCheckBoxChange(e) {

    e.labels.forEach(function (val) {
        if (e.checked) {
            val.classList.add("strikethrough")
        } else {
            val.classList.remove("strikethrough")
        }
    })
}
